// USERS
	{
		"First Name": "John",
		"Last Name": "Doe",
		"Email": "johndoe@gmail.com",
		"Password": "secret123",
		"Is Admin?": No,
		"Mobile Number": "09911234568"
	}


// ORDERS
	{
		"User ID": "johndoe24",
		"Transaction Date": "3/27/2023",
		"Status": "Complete",
		"Total": "1"
	}

// PRODUCTS
	{
		"Name": "Nestle Fresh Milk",
		"Description": "1L of Fresh Milk - pack of 12",
		"Price": "1,497.5 PHP",
		"Stocks": "50 packs",
		"Is Active?": "Yes",
		"SKU": "19A58N2U"
	}

	{
		"Name": "Nestle Low-fat Milk",
		"Description": "1L of Low-Fat - pack of 12",
		"Price": "1,350 PHP",
		"Stocks": "50 packs",
		"Is Active?": "Yes",
		"SKU": "10S58N83Z"
	}

// ORDER PRODUCTS
	{
		"Order ID": "3162",
		"Product ID": "M01",
		"Quantity": "2 Packs",
		"Price": "1,497.5 PHP",
		"Sub Total": "2,995 PHP"
	}


	{
		"Order ID": "3163",
		"Product ID": "M02",
		"Quantity": "1 Pack",
		"Price": "1,350 PHP",
		"Sub Total": "1,350 PHP"
	}



